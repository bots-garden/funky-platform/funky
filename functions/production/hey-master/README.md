

```shell 
# build
kotlinc main.kt -include-runtime -d main.jar

# publish to production
../funky.cli-macos --path=../hey-function \
  --function=hey \
  --branch=master \
  --token=SPACESQUID \
  --platform=http://funky.eu.ngrok.io \
  --cmd=deploy

# invoke
curl http://funky.eu.ngrok.io/function/hey/master

# remove from production
../funky.cli-macos \
  --function=hey \
  --branch=master \
  --token=SPACESQUID \
  --platform=http://funky.eu.ngrok.io \
  --cmd=remove

```