
fun main(args: Array<String>) {
  println("""
    <h1>Hello 🌍</h1>
    <h2>👋 Funky: the little FaaS without container</h2>
    <h3>You just need to deploy a small jar 😃</h3>
  """.trimIndent())
}
