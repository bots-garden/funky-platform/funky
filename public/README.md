## Run

```shell
polymer serve
```


## Build

```shell
polymer build
polymer serve build/es6-bundled
```

test with funky platform:
```
run it inside vagrant
http://funky.test:9090/function/hey/master
```

ADMIN_TOKEN="SPACESQUID" \
nodemon --legacy-watch ./index.js

http://localhost:9090/function/hey/master
http://localhost:9090/function/hello/000
